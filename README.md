# binfmt-support, tools for managing executable binary formats

Git repository: https://gitlab.com/cjwatson/binfmt-support

The `binfmt_misc` kernel module, contained in versions 2.1.43 and later of
the Linux kernel, allows system administrators to register interpreters for
various binary formats based on a magic number or their file extension, and
cause the appropriate interpreter to be invoked whenever a matching file is
executed.  Think of it as a more flexible version of the `#!` executable
interpreter mechanism.

`binfmt-support` provides an `update-binfmts` script with which package
maintainers can register interpreters to be used with this module without
having to worry about writing their own init scripts, and which sysadmins
can use for a higher-level interface to this module.

It also supports a mechanism for extending the range of binary formats among
which the kernel can distinguish with the aid of a userspace helper, called
a detector.

## Installation

If you need to install `binfmt-support` starting from source code, then you
will need these separate packages installed first, as well as a C99
compiler:

 * [pkg-config](https://www.freedesktop.org/wiki/Software/pkg-config)
 * [libpipeline](https://gitlab.com/cjwatson/libpipeline)

See the INSTALL file for general installation instructions.  (If you cloned
`binfmt-support` from git, then run `./bootstrap` to create this file.)

## Bug reporting

You can [report bugs on
GitLab](https://gitlab.com/cjwatson/binfmt-support/-/issues).
